# import the pygame module
import pygame

# import random for random numbers!
import random

# import pygame.locals for easier access to key coordinates
from pygame.locals import *

FPS = 23
fpsclock = pygame.time.Clock()

width = 2000
height = 1143

stride = 10

x = 0
y = 0

x1 = width
y1 = 0

class Player(pygame.sprite.Sprite):
    i = 1

    def __init__(self):
        super(Player, self).__init__()
        self.image = pygame.image.load("png/male/walk_" + str(self.i) + ".png").convert()
        self.image.set_colorkey((0, 0, 0), RLEACCEL)
        self.rect = self.image.get_rect(center = (0, 950))

    def update(self, pressed_keys):
        self.i = ((self.i + 1) % 10) + 1
        self.image = pygame.image.load("png/male/walk_" + str(self.i) + ".png").convert()
        self.image.set_colorkey((0, 0, 0), RLEACCEL)

        #if pressed_keys[K_UP]:
        if pressed_keys[K_LEFT]:
            self.rect.move_ip(-stride, 0)
        if pressed_keys[K_RIGHT]:
            self.rect.move_ip(stride, 0)

        # Keep player on the screen
        if self.rect.left < 0:
            self.rect.left = 0
        elif self.rect.right > width:
            self.rect.right = width
        if self.rect.top <= 0:
            self.rect.top = 0
        elif self.rect.bottom >= height:
            self.rect.bottom = height


class Enemy(pygame.sprite.Sprite):
    i = 1

    def __init__(self):
        super(Enemy, self).__init__()
        self.image = pygame.image.load("png/female/walk_" + str(self.i) + ".png").convert()
        self.image.set_colorkey((0, 0, 0), RLEACCEL)
        self.rect = self.image.get_rect(center=(random.randint(2220, 2300), 950))
        self.speed = random.randint(5, 20)

    def update(self):
        self.i = ((self.i + 1) % 10) + 1
        self.image = pygame.image.load("png/female/walk_" + str(self.i) + ".png").convert()
        self.image.set_colorkey((0, 0, 0), RLEACCEL)

        self.rect.move_ip(-self.speed, 0)
        if self.rect.right < 0:
            self.kill()

# initialize pygame
pygame.init()

# create the screen object
# here we pass it a size of 800x600
screen = pygame.display.set_mode((width, height))

# Create a custom event for adding a new enemy.
ADDENEMY = pygame.USEREVENT + 1
pygame.time.set_timer(ADDENEMY, 5000)

# create our 'player', right now he's just a rectangle
player = Player()

background = pygame.image.load('png/bg/BG.png').convert()

enemies = pygame.sprite.Group()
all_sprites = pygame.sprite.Group()
all_sprites.add(player)

running = True

while running:
    for event in pygame.event.get():
        if event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                running = False
        elif event.type == QUIT:
            running = False
        elif event.type == ADDENEMY:
            new_enemy = Enemy()
            enemies.add(new_enemy)
            all_sprites.add(new_enemy)

    x -= 5
    x1 -= 5

    if x + width - 1 < 0:
        x = width
    if x1 + width - 1 < 0:
        x1 = width

    screen.blit(background, (x, y))
    screen.blit(background, (x1, y1))
    pressed_keys = pygame.key.get_pressed()
    player.update(pressed_keys)
    enemies.update()

    for entity in all_sprites:
        screen.blit(entity.image, entity.rect)

    if pygame.sprite.spritecollideany(player, enemies):
        player.kill()

    pygame.display.flip()

    fpsclock.tick(FPS)
